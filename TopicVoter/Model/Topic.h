//
//  Topic.h
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Topic : NSObject{
    
}

@property(strong, nonatomic)NSString *topicID;
@property(strong, nonatomic)NSString *topicDesc;

@property(strong, nonatomic)NSString *voterID;  

@property(nonatomic)NSInteger totalUpVotes;
@property(nonatomic)NSInteger totalDownVotes;

+(Topic*)getFakeTopic;
+(Topic*)getNewTopicWithDesc:(NSString*)iDesc;

@end
