//
//  Voter.h
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Topic.h"

@interface Voter : NSObject{
    
}

@property(strong, nonatomic)NSString *voterID;
@property(strong, nonatomic)NSString *voteName;
@property(strong, nonatomic)NSMutableArray *topicArray;

-(void)addTopic:(Topic*)iTopic;
+(Voter*)getFakeVoter;

@end
