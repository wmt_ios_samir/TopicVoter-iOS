//
//  Topic.m
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import "Topic.h"

static NSUInteger autoGenID = 0;

@interface Topic(){
    
}
@end

@implementation Topic


#pragma mark - Lifecycle

-(id)init{
    
    self = [super init];
    if(self){
        self.topicID = [@([Topic getAutoGenID]) stringValue];
    }
    return self;
}

-(void)dealloc{
    
}

#pragma mark - Public Interface

+(Topic*)getFakeTopic{
    
    Topic *topic = [[Topic alloc] init];
    
    NSString *descString = [MBFakerLorem sentenceWithNumberOfWords:10];
    descString = [descString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    descString = [descString substringWithRange:NSMakeRange(0, descString.length > KMaxCharacterLimitInTopic ? KMaxCharacterLimitInTopic : descString.length)];
    
    //topic.topicDesc = [MBFakerLorem characters:arc4random_uniform(255)];
    //topic.topicDesc = [MBFakerLorem sentenceWithNumberOfWords:3];
    
    topic.topicDesc = descString;
    
    topic.totalUpVotes = arc4random_uniform(100);
    topic.totalDownVotes = arc4random_uniform(100);
    
    return topic;
}

+(Topic*)getNewTopicWithDesc:(NSString*)iDesc{
    
    Topic *topic = [[Topic alloc] init];
    
    topic.topicDesc = iDesc;
    
    topic.totalUpVotes = 0;
    topic.totalDownVotes = 0;
    
    return topic;
}


#pragma mark - Internal Helpers

+(NSUInteger)getAutoGenID{
    return autoGenID++;
}

@end
