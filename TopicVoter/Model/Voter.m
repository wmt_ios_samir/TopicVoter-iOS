//
//  Voter.m
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import "Voter.h"

static NSUInteger autoGenID = 0;

@interface Voter(){
    
}

+(NSUInteger)getAutoGenID;

@end

@implementation Voter

#pragma mark - Lifecycle

-(id)init{
    
    self = [super init];
    if(self){
        self.voterID = [@([Voter getAutoGenID]) stringValue];
        self.topicArray = [NSMutableArray array];
    }
    return self;
}

-(void)dealloc{
    
}

#pragma mark - Public Interface

-(void)addTopic:(Topic*)iTopic{
    if(iTopic != nil){
        [self.topicArray insertObject:iTopic atIndex:0];
    }
}

+(Voter*)getFakeVoter{
    
    Voter *voter = [[Voter alloc] init];
    voter.voteName = [MBFakerLorem characters:20];
    
    for(NSInteger i=0; i<KMaxFakeTopicsPerVoterLimit; i++){
        Topic *topic = [Topic getFakeTopic];
        topic.voterID = voter.voterID;
        [voter.topicArray insertObject:topic atIndex:0];
    }
    
    return voter;
    
}

#pragma mark - Internal Helpers

+(NSUInteger)getAutoGenID{
    return autoGenID++;
}

@end
