//
//  TopicVotingView.h
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import "BaseView.h"

@interface TopicVotingView : BaseView{
    
}

/**
 Method to Initializing TopicVotingView instance

 @param iTopic : Pass Topic object to topic with votes
 @return TopicVotingView object
 */
-(id)initWithTopic:(Topic*)iTopic;

@end
