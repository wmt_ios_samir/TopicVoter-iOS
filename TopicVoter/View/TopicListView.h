//
//  TopicListView.h
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import "BaseView.h"

typedef NS_OPTIONS(NSInteger, TopicListViewType){
    
    UnknownTopicListViewType = -1,
    Top20TopicListViewType = 1,
    AllRecentTopicListViewType,
    
};

static NSString *topicListCellIdentifier = @"TopicListCell";

@interface TopicListView : BaseView<UITableViewDataSource, UITableViewDelegate>{
    
}


/**
 Method to Initialize TopicListView instance

 @param iTopicListViewType is type of view : Top 20 Topics or All Topics
 @return TopicListView object
 */
-(id)initWithTopicListViewType:(TopicListViewType)iTopicListViewType;


/**
 
 @param event : Set event once table cell is selected
 */
-(void)setTableCellSelectEvent:(TableCellDidSelectEvent)event;


/**
 Method to Reload Table list once any changes are made
 */
-(void)reloadList;


/**
 Method to Display alertviewcontroller to add new topic
 */
-(void)addNewTopic;

@end
