//
//  TopicVotingView.m
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import "TopicVotingView.h"

@interface TopicVotingView(){
    Topic *topic;
    
    UIView *mainView;
    UIView *voteControlView;
    
    UIButton *upvoteButton;
    UILabel *upvoteCountLabel;
    
    UILabel *totalvoteCountLabel;
    
    UIButton *downvoteButton;
    UILabel *downvoteCountLabel;
    
    UITextView *descTextView;
}

-(void)voteButtonAction:(id)sender;
-(void)displayTopic:(Topic*)iTopic;

@end

@implementation TopicVotingView

#pragma mark - Lifecycle

-(id)init{
    
    self = [super init];
    if(self){
        
        [self loadViewControls];
        [self setViewControlsLayout];
    }
    return self;
}

-(id)initWithTopic:(Topic*)iTopic{
    self = [super init];
    
    if(self){
        topic = iTopic;
        [self loadViewControls];
        [self setViewControlsLayout];
        [self displayTopic:topic];
    }
    
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
}

- (void)dealloc{
    
}

#pragma mark - Layout

-(void)loadViewControls{
    [super loadViewControls];
    
    CGFloat iconSize = 80;
    CGFloat edgeMargin = -15;
    CGFloat edgeTopBottomMargin = -25;
    
    /*  mainView Allocation   */
    
    mainView = [[UIView alloc] init];
    [mainView setTranslatesAutoresizingMaskIntoConstraints:false];
    [self addSubview:mainView];
    
    /*  voteControlView Allocation   */
    
    voteControlView = [[UIView alloc] init];
    [voteControlView setTranslatesAutoresizingMaskIntoConstraints:false];
    [mainView addSubview:voteControlView];
    
    /*  upvoteButton Allocation   */
    
    upvoteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [upvoteButton setTranslatesAutoresizingMaskIntoConstraints:false];
    upvoteButton.contentEdgeInsets = UIEdgeInsetsMake(edgeTopBottomMargin, edgeMargin, edgeTopBottomMargin, edgeMargin);
    
    upvoteButton.tag = 1;
    
    FAKIonIcons *icon = [FAKIonIcons androidArrowDropupIconWithSize:iconSize];
    [icon addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor]];
    
    [upvoteButton setAttributedTitle:icon.attributedString forState:UIControlStateNormal];
    [voteControlView addSubview:upvoteButton];

    /*  upvoteCountLabel Allocation   */
    
    upvoteCountLabel = [[UILabel alloc] init];
    [upvoteCountLabel setTranslatesAutoresizingMaskIntoConstraints:false];
    
    [upvoteCountLabel setFont:UIFontFromString(@"AppleSDGothicNeo-Medium;15.0")];
    [upvoteCountLabel setTextColor:UIColorFromRGBWithAlpha(0x000000, 0.8f)];
    
    [upvoteCountLabel setTextAlignment:NSTextAlignmentCenter];
    [voteControlView addSubview:upvoteCountLabel];
    
    /*  totalvoteCountLabel Allocation   */
    
    totalvoteCountLabel = [[UILabel alloc] init];
    [totalvoteCountLabel setTranslatesAutoresizingMaskIntoConstraints:false];
    
    [totalvoteCountLabel setFont:UIFontFromString(@"AppleSDGothicNeo-Bold;15.0")];
    [totalvoteCountLabel setTextColor:UIColorFromRGBWithAlpha(0xffffff, 0.8f)];
    [totalvoteCountLabel setBackgroundColor:UIColorFromRGBWithAlpha(0x000000, 0.6f)];
    [totalvoteCountLabel setClipsToBounds:true];
    
    [totalvoteCountLabel setTextAlignment:NSTextAlignmentCenter];
    [totalvoteCountLabel setNumberOfLines:0];
    
    [voteControlView addSubview:totalvoteCountLabel];
    
    /*  downvoteCountLabel Allocation   */
    
    downvoteCountLabel = [[UILabel alloc] init];
    [downvoteCountLabel setTranslatesAutoresizingMaskIntoConstraints:false];
    
    [downvoteCountLabel setFont:UIFontFromString(@"AppleSDGothicNeo-Medium;15.0")];
    [downvoteCountLabel setTextColor:UIColorFromRGBWithAlpha(0x000000, 0.8f)];
    
    [downvoteCountLabel setTextAlignment:NSTextAlignmentCenter];
    [voteControlView addSubview:downvoteCountLabel];
    
    /*  downvoteButton Allocation   */
    
    downvoteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [downvoteButton setTranslatesAutoresizingMaskIntoConstraints:false];
    downvoteButton.contentEdgeInsets = UIEdgeInsetsMake(edgeTopBottomMargin, edgeMargin, edgeTopBottomMargin, edgeMargin);
    
    downvoteButton.tag = 2;
    
    icon = [FAKIonIcons androidArrowDropdownIconWithSize:iconSize];
    [icon addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor]];
    
    [downvoteButton setAttributedTitle:icon.attributedString forState:UIControlStateNormal];
    [voteControlView addSubview:downvoteButton];
    
    /*  descTextView Allocation   */
    
    descTextView = [[UITextView alloc] init];
    [descTextView setTranslatesAutoresizingMaskIntoConstraints:false];
    
    [descTextView setFont:UIFontFromString(@"AppleSDGothicNeo-Medium;15.0")];
    [descTextView setTextColor:UIColorFromRGBWithAlpha(0x000000, 0.8f)];
    
    [descTextView setEditable:false];
    [mainView addSubview:descTextView];
    
    [upvoteButton addTarget:self action:@selector(voteButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [downvoteButton addTarget:self action:@selector(voteButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [UIUtility setBorderColor:UIColorFromRGBWithAlpha(0x000000, 0.5) width:0.7f andRadius:3.0f ofView:descTextView];
    
    [UIUtility setBorderColor:UIColorFromRGBWithAlpha(0x000000, 0.5) width:0.7f andRadius:14.0f ofView:totalvoteCountLabel];
}

-(void)setViewControlsLayout{
    [super setViewControlsLayout];
    
#if 0
    
    [mainView setBackgroundColor:[UIColor redColor]];
    [voteControlView setBackgroundColor:[UIColor purpleColor]];
    
    [upvoteButton setBackgroundColor:[UIColor greenColor]];
    [upvoteCountLabel setBackgroundColor:[UIColor yellowColor]];
    
    [totalvoteCountLabel setBackgroundColor:[UIColor blueColor]];
    
    [downvoteCountLabel setBackgroundColor:[UIColor greenColor]];
    [downvoteButton setBackgroundColor:[UIColor yellowColor]];
    
    [descTextView setBackgroundColor:[UIColor greenColor]];
    
#endif
    
    [layout expandView:mainView insideView:self];
    
    layout.viewDictionary = NSDictionaryOfVariableBindings(mainView, voteControlView, upvoteButton, upvoteCountLabel, totalvoteCountLabel, downvoteCountLabel, downvoteButton, descTextView);
    
    CGFloat voteControlViewWidth = 90;
    CGFloat descTextViewHeight = 200;
    
    CGFloat totalvoteCountLabelHeight = 25;
    CGFloat labelTopBottomMargin = 15;
    
    layout.metrics = @{@"voteControlViewWidth" : @(voteControlViewWidth),
                       @"descTextViewHeight" : @(descTextViewHeight),
                       @"totalvoteCountLabelHeight" : @(totalvoteCountLabelHeight),
                       @"labelTopBottomMargin" : @(labelTopBottomMargin)
                       };
    
    /*  voteControlView / descTextView Layout    */
    
    layout.control_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[voteControlView(voteControlViewWidth)]-[descTextView]-|" options:0 metrics:layout.metrics views:layout.viewDictionary];
    
    layout.control_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[descTextView(descTextViewHeight)]" options:0 metrics:layout.metrics views:layout.viewDictionary];
    
    [mainView addConstraints:layout.control_H];
    [mainView addConstraints:layout.control_V];
    
    /*  voteControlView Layout    */
    
    layout.position_CenterY = [NSLayoutConstraint constraintWithItem:voteControlView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:descTextView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f];
    
    [mainView addConstraint:layout.position_CenterY];
    
    /*  upvoteButton / upvoteCountLabel / totalvoteCountLabel / downvoteCountLabel / downvoteButton  Layout    */
    
    layout.control_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[upvoteButton]|" options:0 metrics:layout.metrics views:layout.viewDictionary];
    
    layout.control_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[upvoteButton][upvoteCountLabel]-labelTopBottomMargin-[totalvoteCountLabel(totalvoteCountLabelHeight)]-labelTopBottomMargin-[downvoteCountLabel][downvoteButton]|" options:NSLayoutFormatAlignAllLeft | NSLayoutFormatAlignAllRight metrics:layout.metrics views:layout.viewDictionary];
    
    [voteControlView addConstraints:layout.control_H];
    [voteControlView addConstraints:layout.control_V];
}


#pragma mark - Public Interface


#pragma mark - User Interaction

-(void)voteButtonAction:(id)sender{
    
    UIButton *voteButton = (UIButton*)sender;
    
    switch (voteButton.tag) {
        case 1:{
            topic.totalUpVotes += 1;
        }
            break;
            
        case 2:{
            topic.totalDownVotes += 1;
        }
            break;
            
        default:
            break;
    }
    
    [self displayTopic:topic];
    
    NSMutableDictionary *infoDictionary = [NSMutableDictionary dictionary];
    infoDictionary[@"viewType"] = @(Top20TopicListViewType);
    [[VoteManager sharedManager] notifyTopicList:infoDictionary];
    
    infoDictionary[@"viewType"] = @(AllRecentTopicListViewType);
    [[VoteManager sharedManager] notifyTopicList:infoDictionary];
}

#pragma mark - Internal Helpers

-(void)displayTopic:(Topic*)iTopic{
    upvoteCountLabel.text = [AppUtility suffixNumber:@(iTopic.totalUpVotes)];
    downvoteCountLabel.text = [AppUtility suffixNumber:@(iTopic.totalDownVotes)];
    
    totalvoteCountLabel.text = [AppUtility suffixNumber:@(iTopic.totalUpVotes - iTopic.totalDownVotes)];
    descTextView.text = topic.topicDesc;
}

#pragma mark - Server Request


@end
