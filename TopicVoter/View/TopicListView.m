//
//  TopicListView.m
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import "TopicListView.h"

@interface TopicListView(){
    TopicListViewType topicListViewType;
    
    UITableView *topicListTableView;
    NSMutableArray *topicsArray;
    
    NSInteger topicsCount;
    TableCellDidSelectEvent cellSelectEvent;
}

-(void)handleNotification:(NSNotification*)notification;

-(void)setNotificationObserver;
-(void)removeNotificationObserver;

@end

@implementation TopicListView

#pragma mark - Lifecycle

-(id)initWithTopicListViewType:(TopicListViewType)iTopicListViewType{
    
    self = [super init];
    if(self){
        
        topicListViewType = iTopicListViewType;
        
        [self loadViewControls];
        [self setViewControlsLayout];
        [self setNotificationObserver];
    }
    
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
}

- (void)dealloc{
    [self removeNotificationObserver];
}

#pragma mark - Layout

-(void)loadViewControls{
    [super loadViewControls];
    
    
    topicsArray = [NSMutableArray array];
    topicsCount = 0;
    
    /*  topicListTableView Allocation   */
    
    topicListTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    [topicListTableView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [topicListTableView setBackgroundColor:[UIColor clearColor]];
    [topicListTableView setSeparatorColor:UIColorFromRGBWithAlpha(0x000000, 0.5f)];
    
    topicListTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
    topicListTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [topicListTableView setEstimatedRowHeight:90.0f];
    [topicListTableView setRowHeight:UITableViewAutomaticDimension];
    
    [topicListTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
    [topicListTableView setDelegate:self];
    [topicListTableView setDataSource:self];
    
    [self addSubview:topicListTableView];
    [self loadErrorMessageLabel];
    
    [self reloadList];
}

-(void)setViewControlsLayout{
    [super setViewControlsLayout];
    
    [layout expandView:topicListTableView insideView:self];
}

#pragma mark - Public Interface

-(void)setTableCellSelectEvent:(TableCellDidSelectEvent)event{
    cellSelectEvent = event;
}

-(void)reloadList{
    
    [topicsArray removeAllObjects];
    [topicsArray addObjectsFromArray:[[VoteManager sharedManager] getTopicsFromViewType:topicListViewType]];
    
    topicsCount = topicsArray.count;
    [topicListTableView reloadData];
    
    [self displayErrorMessageLabel:nil];
    
    if(topicsCount == 0){
        
        NSString *errorMessage = @"";
        
        switch (topicListViewType) {
            case Top20TopicListViewType:
                errorMessage = @"No topics available.\nPlease enter new topic from all topics section.";
                break;
                
            case AllRecentTopicListViewType:
                errorMessage = @"No topics available.";
                break;
                
            default:
                break;
        }
        
        [self displayErrorMessageLabel:errorMessage];
    }
    
}

-(void)addNewTopic{
    
    NSString *instructionString = [NSString stringWithFormat:@"Input topic description \n\n• Only whitespace not allowed\n• Max. %@ characters allowed", [@(KMaxCharacterLimitInTopic) stringValue]];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle: @"New Topic"
                                                                              message: instructionString
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Topic Description";
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        
        textField.keyboardType = UIKeyboardTypeDefault;
        textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [alertController dismissViewControllerAnimated:true completion:NULL];
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        NSArray * textfields = alertController.textFields;
        UITextField *topicNameTextField = textfields[0];
        
        NSString *topicDesc = @"";
        BOOL isValidData = true;
        
        if(topicNameTextField.hasText){
            
            topicDesc = topicNameTextField.text;
            topicDesc = [topicDesc stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            if(topicDesc.length == 0 || topicDesc.length > 255){
                isValidData = false;
            }
            
        }else{
            isValidData = false;
        }
        
        if(isValidData){
            Topic *topic = [Topic getNewTopicWithDesc:topicNameTextField.text];
            [[VoteManager sharedManager] addNewTopicInArray:topic];
        }
        
    }]];
    
    [[UIUtility getViewControllerForAlertController] presentViewController:alertController animated:true completion:nil];
}

#pragma mark - User Interaction

-(void)handleNotification:(NSNotification*)notification{
    
    NSDictionary *userInfo = notification.userInfo;
    
    if(userInfo != nil){
        TopicListViewType viewType = [userInfo[@"viewType"] integerValue];
        
        switch (viewType) {
                
            case Top20TopicListViewType:{
                [self reloadList];
            }
                break;
                
            case AllRecentTopicListViewType:{
                [self reloadList];
            }
                break;
                
            default:
                break;
        }
        
    }
}

#pragma mark - Internal Helpers

-(void)setNotificationObserver{
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter removeObserver:self
                                  name:KReloadTopicListNotification
                                object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(handleNotification:)
                               name:KReloadTopicListNotification
                             object:[VoteManager sharedManager]];
    
}

-(void)removeNotificationObserver{
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter removeObserver:self
                                  name:KReloadTopicListNotification
                                object:nil];
}

#pragma mark - Server Request

#pragma mark - UITableView DataSource Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return topicsCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = nil;
    
    cell = [tableView dequeueReusableCellWithIdentifier:topicListCellIdentifier];
    
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:topicListCellIdentifier];
    }
    
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    [cell setExclusiveTouch:YES];
    
    [cell.textLabel setFont:UIFontFromString(@"AppleSDGothicNeo-Bold;15")];
    [cell.textLabel setTextColor:UIColorFromRGBWithAlpha(0x000000, 0.8f)];
    
    [cell.textLabel setTextAlignment:NSTextAlignmentLeft];
    [cell.textLabel setNumberOfLines:1];
    [cell.textLabel setLineBreakMode:NSLineBreakByTruncatingTail];
    
    [cell.detailTextLabel setFont:UIFontFromString(@"AppleSDGothicNeo-Medium;13.0")];
    [cell.detailTextLabel setTextColor:UIColorFromRGBWithAlpha(0x000000, 0.8f)];
    
    [cell.detailTextLabel setTextAlignment:NSTextAlignmentLeft];
    [cell.detailTextLabel setNumberOfLines:0];
    
    Topic *topic = topicsArray[indexPath.row];
    NSString *descString = [NSString stringWithFormat:@"%@. %@", [@(indexPath.row + 1) stringValue], topic.topicDesc];
    
    [cell.textLabel setText:descString];
    
    NSString *detailsText = [NSString stringWithFormat:@"▲ Up-votes: %@\n▼ Down-votes: %@", [AppUtility suffixNumber:@(topic.totalUpVotes)], [AppUtility suffixNumber:@(topic.totalDownVotes)]];
    [cell.detailTextLabel setText:detailsText];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor clearColor]];
    
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    [cell.detailTextLabel setBackgroundColor:[UIColor clearColor]];
    
    /*  backgroundView Allocation   */
    
    UIView *backgroundView = cell.backgroundView;
    if(backgroundView == nil){
        backgroundView = [[UIView alloc] init];
        [cell setBackgroundView:backgroundView];
    }
    
    backgroundView.backgroundColor = [UIColor clearColor];
    
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    [cell.detailTextLabel setBackgroundColor:[UIColor clearColor]];
    
    return cell;
}

#pragma mark - UITableView Delegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat cellHeight;
    cellHeight = UITableViewAutomaticDimension;
    
    return cellHeight;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 66;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(cellSelectEvent != nil){
        Topic *topic = topicsArray[indexPath.row];
        cellSelectEvent(indexPath.row, topic);
    }
}


- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setBackgroundColor:[UIColor clearColor]];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


@end
