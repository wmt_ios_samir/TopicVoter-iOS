//
//  UIUtility.m
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import "UIUtility.h"

@implementation UIUtility


+(id)getViewControllerForAlertController{
    id viewController = [[[AppUtility getAppDelegate] window] rootViewController];
    return viewController;
}

+(void)setBorderColor:(UIColor*)borderColor width:(CGFloat)width andRadius:(CGFloat)radius ofView:(UIView*)view{
    
    [view.layer setBorderColor:borderColor.CGColor];
    [view.layer setBorderWidth:width];
    [view.layer setCornerRadius:radius];
}

@end
