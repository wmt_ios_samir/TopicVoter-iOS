//
//  AppUtility.h
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AppUtility : NSObject{
    
}

/*  Object Misc Methods  */

/**
 Method to get AppDelegate instance

 @return AppDelegate object
 */
+(AppDelegate*)getAppDelegate;

/**
 Method to convert in long number with suffixed number format (Ex: 10000 -> 10K)

 @param number : number which want to be suffixed formatted string
 @return String value of suffixed number format
 */
+(NSString*)suffixNumber:(NSNumber*)number;

/*  GCD Methods  */

+(void)executeTaskAfterDelay:(CGFloat)delay completion:(void (^)(void))completionBlock;
+(void)executeTaskInMainThreadAfterDelay:(CGFloat)delay completion:(void (^)(void))completionBlock;

+(void)executeTaskInGlobalQueueWithCompletion:(void (^)(void))completionBlock;
+(void)executeTaskInMainQueueWithCompletion:(void (^)(void))completionBlock;

+(void)executeTaskInGlobalQueueWithSyncCompletion:(void (^)(void))completionBlock;
+(void)executeTaskInMainQueueWithSyncCompletion:(void (^)(void))completionBlock;

@end
