//
//  VoteManager.h
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Voter.h"
#import "TopicListView.h"

typedef NS_OPTIONS(NSInteger, VoteType){
    
    UnknownVoteType = -1,
    UpVoteType = 1,
    DownVoteType,
};

extern NSString *const KReloadTopicListNotification;

@interface VoteManager : NSObject{
    
}

/**
 Method to Initialize (if null) and get singleton object instance of VoteManager

 @return VoteManager object
 */
+(id)sharedManager;

/**
 Method to notify views where Topics are displayed in table once changes are made

 @param infoDictionary : Dictionary to pass type of TopicView
 */
-(void)notifyTopicList:(NSDictionary*)infoDictionary;

/**
 Method to generate Fake voters with fake topics

 @return Array of Fake Voters
 */
-(NSArray*)generateFakeVoters;

/**
 Method to get Fake Voter instance

 @return Fake Voter object with Fake Topics
 */
-(Voter*)getFakeCurrentVoter;

/**
 Method to get voter instance without fake topics list

 @return Voter object without Fake Topics
 */
-(Voter*)getCurrentVoter;

/**
 Method to get topics based on Topic View type

 @param iViewType is type of view : Top 20 Topics or All Topics
 @return Array of Topics
 */
-(NSArray*)getTopicsFromViewType:(TopicListViewType)iViewType;

/**
 Method to add new topic in Voter's topic array

 @param iTopic : Topic instance which will be added to array
 */
-(void)addNewTopicInArray:(Topic*)iTopic;

@end
