//
//  BaseLayout.h
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BaseLayout : NSObject{
    
}

@property(strong, nonatomic)NSLayoutConstraint *position_Top, *position_Bottom;
@property(strong, nonatomic)NSLayoutConstraint *position_Left, *position_Right;

@property(strong, nonatomic)NSLayoutConstraint *margin_Left, *margin_Right;

@property(strong, nonatomic)NSLayoutConstraint *size_Width, *size_Height;
@property(strong, nonatomic)NSLayoutConstraint *position_CenterX, *position_CenterY;

@property(strong, nonatomic)NSArray *control_H, *control_V;
@property(strong, nonatomic)NSDictionary *viewDictionary, *metrics;

/**
 Method to Initialize BaseLayout instance with view

 @param iView : view which will be considered as child view
 @return BaseLayout object
 */
-(id)initWithView:(UIView*)iView;

/**
 Method to exapand view inside mainview using autolayout

 @param mainView : view which will be considered as parent view
 */
-(void)expandViewInsideView:(UIView*)mainView;

/**
 Method to expand containerView (child-view) inside mainview (parent-view)

 @param containerView : view which will be considered as child view
 @param mainView : view which will be considered as parent view
 */
-(void)expandView:(UIView*)containerView insideView:(UIView*)mainView;

@end
