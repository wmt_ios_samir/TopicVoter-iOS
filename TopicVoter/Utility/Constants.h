//
//  Constants.h
//  TopicVoter
//
//  Created by SamSol on 19/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import <Foundation/Foundation.h>

#define KEnabledFakeData 0
#define KMaxTopTopicLimit 20
#define KMaxCharacterLimitInTopic 255
#define KMaxFakeTopicsPerVoterLimit 30

@interface Constants : NSObject

@end
