//
//  AppUtility.m
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import "AppUtility.h"


@implementation AppUtility

#pragma mark - Misc Methods

+(AppDelegate*)getAppDelegate{
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
}

+(NSString*)suffixNumber:(NSNumber*)number{
    
    if (!number)
        return @"";
    
    long long num = [number longLongValue];
    if (num < 1000)
        return [NSString stringWithFormat:@"%lld",num];
    
    int exp = (int) (log(num) / log(1000));
    NSArray * units = @[@"K",@"M",@"G",@"T",@"P",@"E"];
    
    int onlyShowDecimalPlaceForNumbersUnder = 10;
    
    NSString *roundedNumStr = [NSString stringWithFormat:@"%.1f", (num / pow(1000, exp))];
    NSInteger roundedNum = [roundedNumStr integerValue];
    
    if (roundedNum >= onlyShowDecimalPlaceForNumbersUnder){
        
        roundedNumStr = [NSString stringWithFormat:@"%.0f", (num / pow(1000, exp))];
        roundedNum = [roundedNumStr integerValue];
        
    }
    
    if (roundedNum >= 1000){
        exp++;
        roundedNumStr = [NSString stringWithFormat:@"%.1f", (num / pow(1000, exp))];
    }
    
    NSString *result = [NSString stringWithFormat:@"%@%@", roundedNumStr, [units objectAtIndex:(exp-1)]];
    
    NSLog(@"Original number: %@ - Result: %@", number, result);
    return result;
}

#pragma mark - GCD Methods

+(void)executeTaskAfterDelay:(CGFloat)delay completion:(void (^)(void))completionBlock{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        completionBlock();
    });
}

+(void)executeTaskInMainThreadAfterDelay:(CGFloat)delay completion:(void (^)(void))completionBlock{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        completionBlock();
    });
}

+(void)executeTaskInGlobalQueueWithCompletion:(void (^)(void))completionBlock{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        completionBlock();
    });
}

+(void)executeTaskInMainQueueWithCompletion:(void (^)(void))completionBlock{
    dispatch_async(dispatch_get_main_queue(), ^{
        completionBlock();
    });
}

+(void)executeTaskInGlobalQueueWithSyncCompletion:(void (^)(void))completionBlock{
    dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        completionBlock();
    });
}

+(void)executeTaskInMainQueueWithSyncCompletion:(void (^)(void))completionBlock{
    dispatch_sync(dispatch_get_main_queue(), ^{
        completionBlock();
    });
}

@end
