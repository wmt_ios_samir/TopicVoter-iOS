//
//  UIUtility.h
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^TableCellDidSelectEvent)(NSInteger cellIndex, id object);
typedef void (^ControlTouchUpInsideEvent)(id sender, id object);

@interface UIUtility : NSObject

/*  Convert HTML Color Code to UIColor Ex. :0xCECECE    */

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

/*  Convert HTML Color Code with Alpha Value to UIColor Ex. :0xCECECE and Alpha Value Ex.:0.4   */

#define UIColorFromRGBWithAlpha(rgbValue, a) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]

/*  Get UIFont object from Font name and size   */

#define UIFontFromNameAndSize(_name_, _size_) ((UIFont *)[UIFont fontWithName:(NSString *)(_name_) size:(CGFloat)(_size_)])

/*  Get UIFont object from Font name and size seperated by ';'  Ex. : UIFontFromString(@"Arial;10.0f")  */

#define UIFontFromString(fontString) ((UIFont *)[UIFont fontWithName:[fontString componentsSeparatedByString:@";"][0] size:[[fontString componentsSeparatedByString:@";"][1] floatValue]])


/**
 Method to get parent viewcontroller instance for viewcontroller

 @return viewcontroller object
 */
+(id)getViewControllerForAlertController;

/**
 Method to set border on any view

 @param borderColor : UIColor object
 @param width : width of the border
 @param radius : radius of four corners of the border
 @param view : where border will be shown
 */
+(void)setBorderColor:(UIColor*)borderColor width:(CGFloat)width andRadius:(CGFloat)radius ofView:(UIView*)view;

@end
