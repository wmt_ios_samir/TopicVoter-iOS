//
//  VoteManager.m
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import "VoteManager.h"
#import "TopicListView.h"

/*  NSNotification Name Constants     */

NSString *const KReloadTopicListNotification = @"ReloadTopicListNotification";

@interface VoteManager(){
    Voter *voter;
}
@end

@implementation VoteManager


#pragma mark - Lifecycle

-(id)init{
    
    if(self = [super init]){
        
        
    }
    return self;
}

-(void)dealloc{
    
}

#pragma mark - Public Interface

+(id)sharedManager{
    
    static VoteManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

-(void)notifyTopicList:(NSDictionary*)infoDictionary{
    
    NSNotification *notification = [NSNotification notificationWithName:KReloadTopicListNotification
                                                                 object:self
                                                               userInfo:infoDictionary];
    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

-(NSArray*)generateFakeVoters{
    NSMutableArray *fakeVoterArray = [NSMutableArray array];
    
    for(NSInteger i=0; i<arc4random_uniform(50); i++){
        Voter *iVoter = [Voter getFakeVoter];
        [fakeVoterArray addObject:iVoter];
        iVoter = nil;
    }
    
    return fakeVoterArray;
}

-(Voter*)getFakeCurrentVoter{
    voter = [Voter getFakeVoter];
    return voter;
}

-(Voter*)getCurrentVoter{
    
    if(voter == nil){
        voter = [[Voter alloc] init];
    }
    
    return voter;
}

-(NSArray*)getTopicsFromViewType:(TopicListViewType)iViewType{
    
    NSMutableArray *fakeTopicArray = [NSMutableArray array];
    if(voter == nil)
    {
        if(KEnabledFakeData){
            [self getFakeCurrentVoter];
        }else{
            [self getCurrentVoter];
        }
        
    }
    
    [fakeTopicArray removeAllObjects];
    if(voter.topicArray.count > 0){
        
        switch (iViewType) {
                
            case Top20TopicListViewType:{
                
                NSInteger maxLimit = KMaxTopTopicLimit;
                
                NSSortDescriptor *topicUpVoteSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"totalUpVotes" ascending:false];
                NSArray *sortedArray = [voter.topicArray sortedArrayUsingDescriptors:@[topicUpVoteSortDescriptor]];
                
                NSInteger rangeLength = ((sortedArray.count > maxLimit) ? maxLimit : sortedArray.count);
                [fakeTopicArray addObjectsFromArray:[sortedArray subarrayWithRange:NSMakeRange(0, rangeLength)]];
                
            }
                break;
                
            case AllRecentTopicListViewType:{
                [fakeTopicArray addObjectsFromArray:voter.topicArray];
            }
                break;
                
            default:
                break;
        }
        
    }

    return fakeTopicArray;
}

-(void)addNewTopicInArray:(Topic*)iTopic{
    
    if(voter == nil)
    {
        if(KEnabledFakeData){
            [self getFakeCurrentVoter];
        }else{
            [self getCurrentVoter];
        }
    }
    
    [voter addTopic:iTopic];
    
    NSMutableDictionary *infoDictionary = [NSMutableDictionary dictionary];
    infoDictionary[@"viewType"] = @(Top20TopicListViewType);
    [self notifyTopicList:infoDictionary];
    
    infoDictionary[@"viewType"] = @(AllRecentTopicListViewType);
    [self notifyTopicList:infoDictionary];
    
}

#pragma mark - Internal Helpers



@end
