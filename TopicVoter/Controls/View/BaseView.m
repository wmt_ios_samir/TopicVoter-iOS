//
//  BaseView.m
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import "BaseView.h"
#import "BaseLayout.h"

@interface BaseView(){
    
}
@end

@implementation BaseView

#pragma mark - Lifecycle

-(id)init{
    
    self = [super init];
    if(self){
        
    }
    return self;
    
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
}

- (void)dealloc{
    
}


#pragma mark - Layout

-(void)loadViewControls{
    [self setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self setBackgroundColor:[UIColor whiteColor]];
}

-(void)setViewControlsLayout{
    if(layout == nil){
        layout = [[BaseLayout alloc] init];
    }
}

-(void)loadErrorMessageLabel{
    
    /*  errorMessageLabel Allocation   */
    
    errorMessageLabel = [[UILabel alloc] init];
    [errorMessageLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [errorMessageLabel setFont:UIFontFromString(@"AppleSDGothicNeo-Medium;15")];
    [errorMessageLabel setNumberOfLines:0];
    
    [errorMessageLabel setPreferredMaxLayoutWidth:200];
    [errorMessageLabel setTextAlignment:NSTextAlignmentCenter];
    
    [errorMessageLabel setBackgroundColor:[UIColor clearColor]];
    [errorMessageLabel setTextColor:UIColorFromRGBWithAlpha(0x000000, 0.8f)];
    
    [errorMessageLabel setTag:-1];
    
    [self addSubview:errorMessageLabel];
    [self displayErrorMessageLabel:nil];
    
    
    if(layout == nil){
        layout = [[BaseLayout alloc] init];
    }
    
    layout.viewDictionary = NSDictionaryOfVariableBindings(errorMessageLabel);
    layout.metrics = @{@"ControlLeftRightPadding" : @(15.0f)};
    
    /*     errorLabel Layout     */
    
    layout.control_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-ControlLeftRightPadding-[errorMessageLabel]-ControlLeftRightPadding-|" options:0 metrics:layout.metrics views:layout.viewDictionary];
    [self addConstraints:layout.control_H];
    
    layout.control_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-ControlLeftRightPadding-[errorMessageLabel]-ControlLeftRightPadding-|" options:0 metrics:layout.metrics views:layout.viewDictionary];
    [self addConstraints:layout.control_V];
    
    layout.control_H = nil;
    layout.control_V = nil;
    
    layout.viewDictionary = nil;
    layout.metrics = nil;
    
    [self layoutSubviews];
}

#pragma mark - Public Interface

-(void)displayErrorMessageLabel:(NSString*)errorMessage{
    
    [AppUtility executeTaskInMainQueueWithCompletion:^{
        
        if(errorMessageLabel != nil){
            
            [errorMessageLabel setHidden:YES];
            [errorMessageLabel setText:@""];
            
            if(errorMessageLabel.tag == -1){
                [self sendSubviewToBack:errorMessageLabel];
            }
            
            if(errorMessage != nil){
                if(errorMessageLabel.tag == -1){
                    [self bringSubviewToFront:errorMessageLabel];
                }
                
                [errorMessageLabel setHidden:NO];
                [errorMessageLabel setText:errorMessage];
            }
            
            [errorMessageLabel layoutSubviews];
            
        }
    }];
}


#pragma mark - User Interaction


#pragma mark - Internal Helpers


#pragma mark - Server Request


@end
