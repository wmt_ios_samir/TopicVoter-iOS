//
//  BaseView.h
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BaseLayout;

@interface BaseView : UIView{
    BaseLayout *layout;
    UILabel *errorMessageLabel;
}

/**
 Method to initialize BaseView instance

 @return BaseView object
 */
-(id)init;

/**
 Method to load basic components and set basic parameters
 */
-(void)loadViewControls;

/**
 Method to set view layout for basic controls inside view
 */
-(void)setViewControlsLayout;

/**
 Method to load common error label control inside view
 */
-(void)loadErrorMessageLabel;

/**
 Method to display error message in error label

 @param errorMessage : Message string
 */
-(void)displayErrorMessageLabel:(NSString*)errorMessage;

@end
