//
//  BaseNavigationController.m
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import "BaseNavigationController.h"

@interface BaseNavigationController (){
    
}
@end

@implementation BaseNavigationController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setDefaultParameters];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout


#pragma mark - Public Interface

-(void)setDefaultParameters{
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.navigationBar setTranslucent:NO];
    
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

#pragma mark - User Interaction


#pragma mark - Internal Helpers


@end
