//
//  BaseViewController.m
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import "BaseViewController.h"
#import "BaseLayout.h"

@interface BaseViewController (){
    
}

@property(strong, nonatomic)NSString *navigationTitleString;

@end

@implementation BaseViewController

#pragma mark - Lifecycle

- (id)initWithView:(BaseView *)iView{
    self = [self initWithView:iView andNavigationTitle:@""];
    
    if(self){
        aView = iView;
    }
    
    return self;
}

- (id)initWithView:(BaseView *)iView andNavigationTitle:(NSString*)titleString{
    self = [super initWithNibName:nil bundle:nil];
    
    if(self){
        aView = iView;
        _navigationTitleString = titleString;
        
        [self.navigationItem setTitle:_navigationTitleString];
    }
    
    return self;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    
    [self setEdgesForExtendedLayout:UIRectEdgeNone];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self.navigationItem setTitle:_navigationTitleString];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationItem setTitle:_navigationTitleString];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [self.navigationItem setTitle:@""];
    [aView endEditing:YES];

}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (BOOL)prefersStatusBarHidden{
    return false;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}

- (void)dealloc{
    aView = nil;
    _navigationTitleString = nil;
    
    layout = nil;
}


#pragma mark - Layout

-(void)loadViewControls{
    
    [self.view addSubview:aView];
    [self.view setExclusiveTouch:YES];
    [self.view setMultipleTouchEnabled:NO];
}

-(void)setViewControlsLayout{
    
    /*  Layout Allocation   */
    
    layout = [[BaseLayout alloc] initWithView:self.view];
}

-(void)expandViewInsideView{
    [layout expandView:aView insideView:self.view];
}


#pragma mark - Public Interface


#pragma mark - User Interaction


#pragma mark - Internal Helpers


@end
