//
//  BaseNavigationController.h
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavigationController : UINavigationController{
    
}

/**
 Method to set default parameters of navigationcontroller globally
 */
-(void)setDefaultParameters;

@end
