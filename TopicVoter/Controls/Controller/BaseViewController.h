//
//  BaseViewController.h
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BaseView;
@class BaseLayout;

@interface BaseViewController : UIViewController{
    
    BaseLayout *layout;
    BaseView *aView;
    
}

/**
 Method to initialize BaseViewController instance

 @param iView : main view to pass inside viewcontroller
 @return BaseViewController object
 */
-(id)initWithView:(BaseView*)iView;

/**
 Method to initialize BaseViewController instance

 @param iView : main view to pass inside viewcontroller
 @param titleString : Navigation title string to display on viewcontroller
 @return BaseViewController object
 */
- (id)initWithView:(BaseView *)iView andNavigationTitle:(NSString*)titleString;

/**
 Method to load view inside viewcontroller
 */
-(void)loadViewControls;

/**
 Method to set layout of view inside viewcontroller
 */
-(void)setViewControlsLayout;

/**
 Method to expand view inside viewcontroller
 */
-(void)expandViewInsideView;

@end
