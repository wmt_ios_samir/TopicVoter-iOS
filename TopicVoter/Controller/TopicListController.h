//
//  TopicListController.h
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import "BaseViewController.h"
#import "TopicListView.h"

@interface TopicListController : BaseViewController{
    
}


/**
 Method to Initialize TopicListController instance

 @param iTopicListViewType is type of view : Top 20 Topics or All Topics
 @return TopicListController object
 */
-(id)initWithTopicListViewType:(TopicListViewType)iTopicListViewType;

@end
