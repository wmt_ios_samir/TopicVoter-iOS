//
//  TopicListController.m
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import "TopicListController.h"
#import "TopicVotingController.h"

@interface TopicListController (){
    
    TopicListView *topicListView;
    TopicListViewType topicListViewType;
    
}

-(void)showAllTopics;
+(NSString*)getNavigationListFromTopicListViewType:(TopicListViewType)iTopicListViewType;

@end

@implementation TopicListController

#pragma mark - Lifecycle

-(id)init{
    
    id subView = [[TopicListView alloc] init];
    self = [super initWithView:subView];
    
    if(self){
        topicListView = subView;
        [self loadViewControls];
        [self setViewControlsLayout];
    }
    
    subView = nil;
    return self;
}

-(id)initWithTopicListViewType:(TopicListViewType)iTopicListViewType{
    
    id subView = [[TopicListView alloc] initWithTopicListViewType:iTopicListViewType];
    
    NSString *titleString = [TopicListController getNavigationListFromTopicListViewType:iTopicListViewType];
    self = [super initWithView:subView andNavigationTitle:titleString];
    
    if(self){
        topicListView = subView;
        topicListViewType = iTopicListViewType;
        
        [self loadViewControls];
        [self setViewControlsLayout];
    }
    
    subView = nil;
    return self;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    
}

#pragma mark - Layout

-(void)loadViewControls{
    [super loadViewControls];
    
    /*  addTopicBarButton Allocation   */
    
    FAKFontAwesome *icon = nil;
    UIBarButtonItem *barButton = nil;
    UIImage *iconImage = nil;
    
    if(topicListViewType == Top20TopicListViewType){
        
        /*  barButton Allocation   */
        
        icon = [FAKFontAwesome listIconWithSize:20];
        [icon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
        
        iconImage = [icon imageWithSize:CGSizeMake(20, 20)];
        icon.iconFontSize = 15;
        
        barButton = [[UIBarButtonItem alloc] initWithImage:iconImage
                                                     style:UIBarButtonItemStylePlain
                                                    target:self
                                                    action:@selector(showAllTopics)];
        
        self.navigationItem.rightBarButtonItems = @[barButton];
        
        
    }else if(topicListViewType == AllRecentTopicListViewType){
        
        /*  addTopicBarButton Allocation   */
        
        icon = [FAKFontAwesome plusCircleIconWithSize:20];
        [icon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
        
        iconImage = [icon imageWithSize:CGSizeMake(20, 20)];
        icon.iconFontSize = 15;
        
        UIBarButtonItem *addTopicBarButton = [[UIBarButtonItem alloc] initWithImage:iconImage
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:topicListView
                                                                             action:@selector(addNewTopic)];
        
        /*  barButton Allocation   */
        
        icon = [FAKFontAwesome homeIconWithSize:20];
        [icon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
        
        iconImage = [icon imageWithSize:CGSizeMake(20, 20)];
        icon.iconFontSize = 15;
        
        barButton = [[UIBarButtonItem alloc] initWithImage:iconImage
                                                     style:UIBarButtonItemStylePlain
                                                    target:self
                                                    action:@selector(showTop20Topics)];
        
        self.navigationItem.rightBarButtonItems = @[addTopicBarButton, barButton];
        
    }
    
    __block TopicListController *blockSelf = self;
    [topicListView setTableCellSelectEvent:^(NSInteger cellIndex, id object) {
       
        if(object != nil){
            Topic *topic = (Topic*)object;
            TopicVotingController *viewController = [[TopicVotingController alloc] initWithTopic:topic];
            [blockSelf.navigationController pushViewController:viewController animated:true];
            viewController = nil;
        }
        
    }];
    
}

-(void)setViewControlsLayout{
    [super setViewControlsLayout];
    [super expandViewInsideView];
}


#pragma mark - Public Interface


#pragma mark - User Interaction

-(void)showAllTopics{
    
    TopicListController *viewcontroller = [[TopicListController alloc] initWithTopicListViewType:AllRecentTopicListViewType];
    [self.navigationController pushViewController:viewcontroller animated:true];
    viewcontroller = nil;
    
}

-(void)showTop20Topics{
    [self.navigationController popViewControllerAnimated:true];
}


#pragma mark - Internal Helpers

+(NSString*)getNavigationListFromTopicListViewType:(TopicListViewType)iTopicListViewType{
    
    NSString *titleString = @"";
    
    switch (iTopicListViewType) {
            
        case Top20TopicListViewType:
            titleString = @"Top 20 Topics";
            break;
            
        case AllRecentTopicListViewType:
            titleString = @"All Topics";
            break;
            
        default:
            break;
    }
    
    return titleString;
    
}


#pragma mark - Server Request



@end
