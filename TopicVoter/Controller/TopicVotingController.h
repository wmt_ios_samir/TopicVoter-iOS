//
//  TopicVotingController.h
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import "BaseViewController.h"

@interface TopicVotingController : BaseViewController{
    
}

/**
 Method to Initialize TopicVotingController instance

 @param iTopic : Pass Topic object to topic with votes
 @return TopicVotingController object
 */

-(id)initWithTopic:(Topic*)iTopic;

@end
