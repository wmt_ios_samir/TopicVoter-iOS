//
//  TopicVotingController.m
//  TopicVoter
//
//  Created by SamSol on 18/04/18.
//  Copyright © 2018 WMT. All rights reserved.
//

#import "TopicVotingController.h"
#import "TopicVotingView.h"

@interface TopicVotingController (){

    TopicVotingView *topicVotingView;
    Topic *topic;
    
}
@end

@implementation TopicVotingController

#pragma mark - Lifecycle

-(id)init{
    
    id subView = [[TopicVotingView alloc] init];
    self = [super initWithView:subView];
    
    if(self){
        topicVotingView = subView;
        [self loadViewControls];
        [self setViewControlsLayout];
    }
    
    subView = nil;
    return self;
    
}

-(id)initWithTopic:(Topic*)iTopic{
    
    id subView = [[TopicVotingView alloc] initWithTopic:iTopic];
    self = [super initWithView:subView andNavigationTitle:@"Vote Topic"];
    
    if(self){
        topicVotingView = subView;
        [self loadViewControls];
        [self setViewControlsLayout];
    }
    
    subView = nil;
    return self;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    
}

#pragma mark - Layout

-(void)loadViewControls{
    [super loadViewControls];
    
}

-(void)setViewControlsLayout{
    [super setViewControlsLayout];
    [super expandViewInsideView];
}


#pragma mark - Public Interface


#pragma mark - User Interaction


#pragma mark - Internal Helpers


#pragma mark - Server Request



@end
